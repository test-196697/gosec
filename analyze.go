package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/ruleset"
)

const (
	pathPkg           = "app"
	pathGoSrc         = "/go/src"
	pathGoPkg         = pathGoSrc + "/" + pathPkg
	pathOutput        = "/tmp/gosec.json"
	pathGosec         = "/bin/gosec"
	envVarGoSecConfig = "SAST_GOSEC_CONFIG"
	flagGoSecConfig   = "gosec-config"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		cli.StringFlag{
			Name:   flagGoSecConfig,
			Usage:  "Relative path to a gosec config file",
			EnvVar: envVarGoSecConfig,
		},
	}
}

func analyze(c *cli.Context, projectPath string) (io.ReadCloser, error) {
	var cmd *exec.Cmd
	var err error

	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Env = os.Environ()
		return cmd
	}

	// Load custom config if available
	rulesetPath := filepath.Join(projectPath, ruleset.PathSAST)
	rule, err := ruleset.Load(rulesetPath, "gosec")
	if err != nil {
		switch err.(type) {
		case *os.PathError:
			// Couldn't load an optional custom ruleset file
			log.Debug(err)
		default:
			// TODO: replace with type check
			// The ruleset file did not include a `gosec` analyzer section.
			if strings.HasPrefix(err.Error(), "unable to find custom rule") {
				log.Debug(err)
			} else {
				return nil, err
			}
		}
	}

	// We don't control the directory where the source code is mounted
	// but Go requires the code to be within $GOPATH.
	// We could create a symlink but that wouldn't work with Gosec,
	// so we have to copy all the project source code
	// to some directory under $GOPATH/src.
	// TODO: make it possible to specify the exact path of the package.
	// FIXME: this copy should be necessary when go modules are disabled
	log.Info("Copying modules into path...")
	cmd = setupCmd(exec.Command("cp", "-r", projectPath, pathGoPkg))
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		return nil, err
	}

	// Gosec needs the dependency to be fetched.
	log.Info("Fetching dependencies...")
	cmd = setupCmd(exec.Command("go", "get", "./..."))
	cmd.Dir = pathGoPkg
	output, err = cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		return nil, err
	}

	// Set up basic gosec arguments
	gosecArgs := []string{"-fmt=json", "-out=" + pathOutput, "./..."}

	// Check if SAST_GOSEC_CONFIG is defined and points to a file
	configFile := c.String(flagGoSecConfig)
	if configFile != "" {
		configPath := filepath.Join(projectPath, configFile)
		if err := checkConfig(configPath); err != nil {
			return nil, err
		}

		// Prepend -conf PATH to the arguments for gosec
		gosecArgs = append([]string{"-conf", configPath}, gosecArgs...)
	} else if rule != nil && len(rule.PassThrough) != 0 {
		passThrough := rule.PassThrough[0]
		passThroughArgs, err := processPassthrough(projectPath, passThrough)
		if err != nil {
			return nil, err
		}
		if passThrough.Type == ruleset.PassThroughRaw {
			defer os.Remove(gosecArgs[1])
		}
		gosecArgs = append(passThroughArgs, gosecArgs...)
	}

	log.Info("Running gosec...")
	cmd = setupCmd(exec.Command(pathGosec, gosecArgs...))
	cmd.Dir = pathGoPkg
	output, err = cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	// NOTE: Gosec exit with status 1 if some vulnerabilities have been found.
	if err != nil && cmd.ProcessState.ExitCode() > 1 {
		return nil, err
	}
	return os.Open(pathOutput)
}

// processPassthrough processes the ruleset and returns the set of args to appended
// to the analyzer command
func processPassthrough(projectPath string, passThrough ruleset.PassThrough) ([]string, error) {
	value := passThrough.Value
	if passThrough.Type == ruleset.PassThroughFile {
		return passthroughFile(projectPath, value)
	} else if passThrough.Type == ruleset.PassThroughRaw {
		return passthroughRaw(value)
	} else {
		return []string{}, nil
	}
}

// passthroughFile returns a set of args identifying the gosec configuration file
func passthroughFile(projectPath string, filename string) ([]string, error) {
	configPath := filepath.Join(projectPath, cleanPath(filename))
	if err := checkConfig(configPath); err != nil {
		return nil, err
	}

	log.Infof("Loading config from custom ruleset passthrough file: %s\n", filename)
	// Prepend -conf PATH to the arguments for gosec
	return []string{"-conf", configPath}, nil
}

// passthroughRaw writes raw configuration to a file and returns a set of args
// identifying the gosec configuration file
func passthroughRaw(raw string) ([]string, error) {
	// create tmp config for raw values from the passthrough to be loaded into
	tmpConfig, err := ioutil.TempFile("", "gosec-config*.json")
	if err != nil {
		return nil, fmt.Errorf("failed to create tmp gosec-config file")
	}

	if _, err = tmpConfig.Write([]byte(raw)); err != nil {
		return nil, fmt.Errorf("failed to write to create tmp gosec-config file")
	}
	log.Info("Loading config from custom ruleset via raw passthrough")

	// Prepend -conf PATH to the arguments for gosec
	return []string{"-conf", tmpConfig.Name()}, nil
}

// CleanPath makes a path safe for use with filepath.Join. This is done by not
// only cleaning the path, but also (if the path is relative) adding a leading
// '/' and cleaning it (then removing the leading '/'). This ensures that a
// path resulting from prepending another path will always resolve to lexically
// be a subdirectory of the prefixed path. This is all done lexically, so paths
// that include symlinks won't be safe as a result of using CleanPath.
//
// This function comes from runC (libcontainer/utils/utils.go):
// https://github.com/opencontainers/runc/blob/d636ad6256f9194b0f4c6ee181e75fb36e3446d8/libcontainer/utils/utils.go#L53
func cleanPath(path string) string {
	// Deal with empty strings nicely.
	if path == "" {
		return ""
	}

	// Ensure that all paths are cleaned (especially problematic ones like
	// "/../../../../../" which can cause lots of issues).
	path = filepath.Clean(path)

	// If the path isn't absolute, we need to do more processing to fix paths
	// such as "../../../../<etc>/some/path". We also shouldn't convert absolute
	// paths to relative ones.
	if !filepath.IsAbs(path) {
		path = filepath.Clean(string(os.PathSeparator) + path)
		// This can't fail, as (by definition) all paths are relative to root.
		path, _ = filepath.Rel(string(os.PathSeparator), path)
	}

	// Clean the path again for good measure.
	return filepath.Clean(path)
}

func checkConfig(configPath string) error {
	st, err := os.Stat(configPath)
	if err != nil {
		return err
	} else if st.IsDir() {
		return fmt.Errorf("%q is a directory", configPath)
	}
	return nil
}
