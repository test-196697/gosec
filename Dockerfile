FROM golang:1.15-alpine AS build

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-2.4.0}
ARG GOSEC_SHA1SUM=f75bd727b0e1d4673b1f72a174c5f4f954ac63e9

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/buildapp
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

ADD https://github.com/securego/gosec/releases/download/v${SCANNER_VERSION}/gosec_${SCANNER_VERSION}_linux_amd64.tar.gz /tmp/gosec.tar.gz
RUN tar xf /tmp/gosec.tar.gz && \
  echo "$GOSEC_SHA1SUM  /tmp/gosec.tar.gz" | sha1sum -c && \
  tar xf /tmp/gosec.tar.gz && \
  rm -f /tmp/gosec.tar.gz && \
  mv gosec /bin/gosec

# Create new base container with a clean $GOPATH
FROM golang:1.15-alpine AS base

RUN apk --no-cache add git ca-certificates gcc libc-dev

COPY --from=build /analyzer /analyzer
COPY --from=build /bin/gosec /bin/gosec

ENTRYPOINT []
CMD ["/analyzer", "run"]
