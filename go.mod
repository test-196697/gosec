module gitlab.com/gitlab-org/security-products/analyzers/gosec/v2

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/urfave/cli v1.22.4
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.19.1
	gitlab.com/gitlab-org/security-products/cwe-info-go v1.0.2
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
)

go 1.15
